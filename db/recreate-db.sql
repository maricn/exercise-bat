CREATE EXTENSION cube;
CREATE EXTENSION earthdistance;
CREATE EXTENSION pgcrypto;

-- Table: cleaners

DROP TABLE IF EXISTS cleaners;

CREATE TABLE cleaners
(
  id           CHARACTER VARYING(42)  NOT NULL DEFAULT gen_random_uuid(),
  name         CHARACTER VARYING(255) NOT NULL,
  country_code CHARACTER VARYING(2)   NOT NULL,
  latitude     REAL                   NOT NULL,
  longitude    REAL                   NOT NULL,
  gender       CHARACTER VARYING(1),
  preferences  CHARACTER VARYING(255),
  CONSTRAINT firstkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE cleaners
OWNER TO bookatiger;