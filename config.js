var config = {};

config.logFilename = './logfile.log';

// Distances per countries (in km)
config.distances = {};
config.distances['de'] = 10;
config.distances['at'] = 20;
config.distances['ch'] = 17.5;
config.distances['nl'] = 9;
config.countries = config.distances; // alias for set-like 'in operator' behaviour

// Database config (in 'pg' format)
config.database = {
    'host': 'localhost',
    'port': 5432,
    'user': 'bookatiger',
    'password': 'bookatiger',
    'database': 'bookatiger',
    'max': 10
};
config.databaseRecreate = {
    'host': 'localhost',
    'port': 5432,
    'database': 'postgres',
    'user': 'bookatiger',
    'password': 'bookatiger'
};

module.exports = config;