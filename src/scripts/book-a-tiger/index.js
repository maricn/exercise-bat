var conf = require('../../../config');

var pg = require('pg');
var pgClient = new pg.Client(conf.database);

var winston = require('winston');
// @NOTE: toggle log level
winston.level = 'info';
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {'timestamp': true});
winston.add(winston.transports.File, {'timestamp': true, 'filename': conf.logFilename});

var BookATiger = function () {
    var validate = function (args) {
        return args.country in conf.countries &&
            args.geo.latitude <= 90 && args.geo.latitude >= -90 &&
            args.geo.longitude <= 180 && args.geo.longitude >= -180 &&
            (args.gender === undefined || args.gender in {'M': true, 'F': true}) &&
            (args.preferences === undefined || args.preferences.length <= 255);
    };

    return {
        getMatches: function (args, callback) {
            winston.log('debug', 'getMatches(' + JSON.stringify(args) + ')');

            // Check input arguments
            if (validate(args)) {

                // Connect to postgresql
                pgClient.connect(function (err) {
                    if (err) callback(err);

                    // Construct a query filtering cleaners by country and distance
                    var query = "SELECT c FROM cleaners c WHERE " +
                        " c.country_code = $1 AND " +
                        " earth_box(ll_to_earth($2, $3), $4) @> ll_to_earth(c.latitude, c.longitude) ";
                    var queryParams = [
                        args.country,
                        args.geo.latitude,
                        args.geo.longitude,
                        conf.distances[args.country] * 1000
                    ];

                    // If gender is specified, include it in query
                    if (args.gender !== undefined) {
                        query += " AND c.gender = $5 ";
                        queryParams.push(args.gender);
                    }

                    // If some preferences are specified, include those too
                    if (args.preferences !== undefined) {
                        query += " AND ('{'||c.preferences||'}')::text[] @> $" + (queryParams.length + 1).toString();
                        queryParams.push('{' + args.preferences + '}');
                    }

                    // Execute query with callback
                    pgClient.query(query, queryParams,
                        function (err, result) {
                            if (err) callback(err);

                            // Disconnect the client
                            pgClient.end(function (err) {
                                if (err) callback(err);
                            });

                            // Write to debug and execute callback with array of results
                            winston.log('debug', JSON.stringify(result));
                            if (callback && result.rowCount > 0) {
                                callback(null, result.rows);
                            }
                        });
                });
            } else {
                callback(new Error("Validation of arguments failed!"));
            }
        }
    }
};

module.exports.BookATiger = new BookATiger();

