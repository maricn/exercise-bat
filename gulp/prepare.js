var gulp = require('gulp');
var conf = require('../config');
var faker = require('faker');
var pg = require('pg');
var fs = require('fs');

var winston = require('winston');
winston.level = 'info';

gulp.task('recreate-db', function () {
    new pg.Client(conf.databaseRecreate).connect(function (err, client) {
        if (err) throw err;
        winston.log('debug', "Dropping database...");
        client.query("DROP DATABASE bookatiger", function (err) {
            if (err) winston.log('error', err);
            winston.log('debug', "Database dropped or didn't exist.");
            client.query("CREATE DATABASE bookatiger", function (err) {
                if (err) throw err;
                winston.log('debug', "Database recreated!");
                winston.log('debug', "Releasing connection (db change).");
                client.end(function (err) {
                    if (err) throw err;
                });

                var sql = fs.readFileSync('./db/recreate-db.sql').toString();
                new pg.Client(conf.database).connect(function (err, client) {
                    if (err) throw err;
                    winston.log('debug', "Running recreate-db.sql file...");
                    client.query(sql, function (err) {
                        if (err) throw err;
                        winston.log('debug', "Done running recreate-db.sql file. Releasing connection.");
                        client.end(function (err) {
                            if (err) throw err;
                        })
                    });
                });
            });
        });
    });
});

gulp.task('populate-db', function () {
    var pool = new pg.Pool(conf.database);

    function insert(args) {
        winston.log('info', JSON.stringify(args));
        pool.query("INSERT INTO cleaners (name, country_code, latitude, longitude, gender, preferences) " +
            " VALUES ($1, $2, $3, $4, $5, $6)", args, function (err) {
            if (err) throw err;
        });
    }

    var countries = Object.keys(conf.countries);
    for (var i = 0; i < 1000; i++) {
        insert([
            faker.name.findName(),
            countries[countries.length * Math.random() << 0],
            parseFloat(faker.address.latitude()),
            parseFloat(faker.address.longitude()),
            i % 2 ? 'M' : 'F',
            faker.commerce.product().toLowerCase() + ',' +
            faker.commerce.product().toLowerCase() + ',' +
            faker.commerce.product().toLowerCase() + ',' +
            faker.commerce.product().toLowerCase()
        ]);
    }

    // Releasing pool
    pool.end(function (err) {
        if (err) throw err;
    });
});