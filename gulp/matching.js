var gulp = require('gulp');
var bookATiger = require('../src/scripts/book-a-tiger').BookATiger;

var winston = require('winston');
winston.level = 'info';

gulp.task('get-matches', function () {
    function parseGeo(geo, cb) {
        if (geo !== undefined && geo.split(',').length == 2) {
            var lat = parseFloat(geo.split(',')[0]);
            var long = parseFloat(geo.split(',')[1]);
            if ((lat >= -90 && lat <= 90) && (long >= -180 && long <= 180)) {
                return cb(null, {'latitude': lat, 'longitude': long});
            }
        }
        cb(new Error('Error parsing geo data.'), null);
    }

    var argv = require('yargs')
        .usage('Usage: $0 --country=country_code --geo=latitude,longitude [--gender=M/F] [--preferences=pref1,pref2...]')
        .alias('c', 'country')
        .alias('p', 'preferences')
        .demand(['country', 'geo'])
        .argv;

    parseGeo(argv.geo, function (err, geo) {
        if (err) throw err;

        var argObject = {
            'country': typeof(argv.country) === 'string' ? argv.country.toLowerCase() : argv.country.toString().toLowerCase(),
            'geo': geo
        };
        if (argv.gender !== undefined) {
            argObject.gender = argv.gender.toUpperCase();
        }
        if (argv.preferences !== undefined) {
            argObject.preferences = argv.preferences.toLowerCase();
        }

        bookATiger.getMatches(argObject, function (err, data) {
            if (err) throw err;

            for (var i = 0; i < data.length; i++) {
                winston.log('info', data[i]);
            }
        });
    });
});
