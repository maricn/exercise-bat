# Exercise Book-A-Tiger

## Prerequisites

Installed Node.JS and PostgreSQL database.

## Running
 
 * Run `npm install .` from project directory.
 * \[DB setup\] Run PostgreSQL, create `bookatiger` user with superuser privileges (i know..) and setup config.js database connection parameters accordingly if necessary.
 * \[DB setup\] Execute `gulp recreate-db` to recreate database and prepare it for queries.
 * \[DB setup\] Execute `gulp populate-db` to populate database with random data.
 * Execute `gulp get-matches` for further running instructions.
 
In order to test manually - populate db multiple times, adjust distances for countries and try querying cleaners.  